function [ varargout ] = dailyAlloDiscr( bhv, hFig )
%daily_report Generates a session report figure
% input argument = output of parsedata function;
%%
narginchk(1,2)

if ~isstruct(bhv)
    bhv = parseAlloDiscr(bhv);
end
if nargin < 2
    hFig = figure('windowstyle','docked');
else
    figure(hFig)
    clf
end

annotation('textbox',[.55 .5 .5 .5],'edgecolor','none','String',[bhv.Animal ' ' bhv.Date ' ' bhv.Protocol 'v' bhv.ProtocolVersion],'fontsize',15,'fontname','Arial','interpreter','none');
str = ['Performance \nOverall:\t' sprintf('%2.1f',nansum(bhv.ChoiceCorrect)/length(bhv.ChoiceCorrect)*100)...
    '%%\nValid only:\t' sprintf('%2.1f',nanmean(bhv.ChoiceCorrect)*100) '%%\n\n'];
fprintf(str)

myxtick = [0 .33 .5 .67 1];
myxticklabel = {'','','','',''};
myytick = [0 .5 1];
myyticklabel = {'','',''};

%% Trial history
subplot(1,4,1), hold on, cla
try
    %  Blocks
    myalpha = 1;
    
    if ~isfield(bhv,'LeftLong')
        bhv.LeftLong = ones(size(bhv.ChoiceLeft));
    end
    if ~isfield(bhv,'ChoiceLong')
        bhv.ChoiceLong = bhv.ChoiceLeft;
    end
    if ~isfield(bhv,'BlockNumber')
        bhv.BlockNumber = ones(size(bhv.ChoiceLeft));
    end
    paleColor = [188 191 239; 250 195 192]/255;
    satColor = rgb2hsv(paleColor);
    satColor(:,2) = 1;
    satColor = hsv2rgb(satColor);
    
    for iTrial = 1:length(bhv.LeftLong)
%         if iTrial == 1
%             boxBegin = iTrial;
%         elseif iTrial == length(bhv.LeftLong)
%             tNdx = find(~isnan(bhv.LeftLong),1,'last');
%             patch([0 1 1 0]*bhv.Scaling,[ones(1,2)*boxBegin-.5 ones(1,2)*iTrial+.5],paleColor(2-bhv.LeftLong(tNdx),:),'edgecolor','none','facealpha',myalpha)
%         else
%             if ~any(isnan(bhv.LeftLong(iTrial-1:iTrial))) && bhv.LeftLong(iTrial)~=bhv.LeftLong(iTrial-1);
%                 patch([0 1 1 0]*bhv.Scaling,[ones(1,2)*boxBegin ones(1,2)*iTrial]-.5,paleColor(2-bhv.LeftLong(iTrial-1),:),'edgecolor','none','facealpha',myalpha)
%                 boxBegin = iTrial;
%             end
%         end
        if ~isnan(bhv.LeftLong(iTrial))
            patch([0 1 1 0]*bhv.Scaling,[ones(1,2)*(iTrial-.5) ones(1,2)*iTrial+.5],...
                paleColor(2-bhv.LeftLong(iTrial),:),'edgecolor','none','facealpha',myalpha)
        end
    end
    
    %  Valid trials
    valid_ndx = ~isnan(bhv.ChoiceCorrect);
    correct_ndx = bhv.ChoiceCorrect==1;
    incorrect_ndx = bhv.ChoiceCorrect==0;
    plot(bhv.Interval(correct_ndx & valid_ndx)*bhv.Scaling,find(correct_ndx & valid_ndx),'.g','markersize',10)
    plot(bhv.Interval(incorrect_ndx & valid_ndx)*bhv.Scaling,find(incorrect_ndx & valid_ndx),'.r','markersize',10)
    
    plot(bhv.Interval(correct_ndx & ~valid_ndx)*bhv.Scaling,find(correct_ndx & ~valid_ndx),'og','markersize',5)
    plot(bhv.Interval(incorrect_ndx & ~valid_ndx)*bhv.Scaling,find(incorrect_ndx & ~valid_ndx),'or','markersize',5)
    
    %  Other trials
    
    if isfield(bhv,'BrokeFix')
        broke_ndx = find(bhv.BrokeFix);
        plot(bhv.BrokeFixTime(broke_ndx),broke_ndx,'x','color',ones(1,3)*0,'markersize',3);
    else
        prem_long_ndx = find(bhv.PrematureLong==1);
        prem_short_ndx = find(bhv.PrematureLong==0);
        prem_ndx = bhv.Premature==1;
        plot(bhv.PremTime(prem_long_ndx),prem_long_ndx,'*','color',[.5 .5 .5],'markersize',3);
        plot(bhv.PremTime(prem_short_ndx),prem_short_ndx,'*k','markersize',3);
    end
    
    axis([0 bhv.Scaling .5 .5+length(bhv.TrialNumber)])
    ax(1) = gca;
    set(ax(1),'box','off','tickdir','out','YDir','reverse','xtick',myxtick,'xticklabel',myxticklabel)
    ylabel Trial; xlabel 'Interval duration (s)'
    %axis tight
    
    %  Running average of performance
    windowSize = 30;
    kernel = ones(1,windowSize)/windowSize;
        
    %line(filter(kernel,1,bhv.ChoiceCorrect(valid_ndx|cued_ndx)),find((valid_ndx|cued_ndx)),'LineWidth',2,'color',[0.0078 0.6784 0.9216],'Parent',ax(1));
    line([ones(1,2)*bhv.Scaling/2],[0 length(bhv.ChoiceCorrect)],'color','k','linestyle','--','LineWidth',1,'Parent',ax(1));
    if numel(bhv.ChoiceCorrect) > windowSize
        avgTotal = filter(kernel,1,bhv.ChoiceCorrect==1);
        avgTotal(1:windowSize) = nan;
        line(avgTotal*bhv.Scaling,1:length(avgTotal),'LineWidth',2,'color',ones(1,3)*.5,'Parent',ax(1));
    end
    if numel(bhv.ChoiceCorrect(valid_ndx)) > windowSize
        avgValid = filter(kernel,1,bhv.ChoiceCorrect(valid_ndx));
        avgValid(1:windowSize) = nan;
        line(avgValid*bhv.Scaling,find(valid_ndx),'LineWidth',2,'color',[0.0078 0.6784 0.9216],'Parent',ax(1));
    end
    title 'Trial history'
catch
    warning('Error on trial history section')
end
%% Psychometric curves
xaxis = linspace(min(bhv.IntervalSet),max(bhv.IntervalSet),100)*bhv.Scaling;
subplot(3,4,2), axis square
hold on
cla

try
    [psycK, IntervalSetK] = psycurve(bhv);
    betaK = glmfit(bhv.IntervalPrecise(:),bhv.ChoiceLong(:),'binomial');
    ycontK = 1./(1+exp(-1*(betaK(1)+xaxis*betaK(2))));
    plot(xaxis,ycontK,'color',[ones(1,3)*.5],'linewidth',2)
    
    ndxB = bhv.LeftLong == 1;
    if ~isempty(find(ndxB,1))
        [psycB, IntervalSetB] = psycurve(bhv.Interval(ndxB),bhv.ChoiceLong(ndxB));
        betaB = glmfit(bhv.IntervalPrecise(ndxB)',bhv.ChoiceLong(ndxB)','binomial');
        ycontB = 1./(1+exp(-1*(betaB(1)+xaxis*betaB(2))));
        plot(xaxis,ycontB,'color',paleColor(1,:),'linewidth',2)
    end
    
    ndxR = bhv.LeftLong == 0;
    if ~isempty(find(ndxR,1))
        [psycR, IntervalSetR] = psycurve(bhv.Interval(ndxR),bhv.ChoiceLong(ndxR));
        betaR = glmfit(bhv.IntervalPrecise(ndxR)',bhv.ChoiceLong(ndxR)','binomial');
        ycontR = 1./(1+exp(-1*(betaR(1)+xaxis*betaR(2))));
        plot(xaxis,ycontR,'color',paleColor(2,:),'linewidth',2)
    end
    
    plot(IntervalSetK*bhv.Scaling, psycK,'k.','markersize',20)
    if ~isempty(find(ndxB,1))
        plot(IntervalSetB*bhv.Scaling, psycB,'.','color',satColor(1,:),'markersize',20)
    end
    if ~isempty(find(ndxR,1))
        plot(IntervalSetR*bhv.Scaling, psycR,'.','color',satColor(2,:),'markersize',20)
    end
    
    ylabel 'P(long)'
    % xlabel 'Time (s)'
    text(min(xaxis),1,'LeftLong','color',satColor(1,:),'verticalalignment','top')
    text(min(xaxis),.9,'LeftShort','color',satColor(2,:),'verticalalignment','top')
    text(min(xaxis),.8,'All','color',zeros(1,3),'verticalalignment','top')
    
    margins;
    set(gca,'ytick',[0 .5 1],'ylim',[-.05 1.05],'xtick',bhv.IntervalSet*bhv.Scaling,'xticklabel',bhv.IntervalSet*bhv.Scaling/1000,'tickdir','out')
    a=get(gca,'XTickLabel');
    set(gca,'XTickLabel',[]);
    b=get(gca,'XTick');
    c=get(gca,'YTick');
    th=text(b,ones(size(b))*-.1,a,'HorizontalAlignment','right','rotation',90);
    clear a b c th
    title Psych
end
%% Histogram of trial types
subplot(3,4,3), axis square, hold on
cla
try
    % bar(bhv.IntervalSet*bhv.Scaling,histc(bhv.Interval(bhv.LeftLong==1),bhv.IntervalSet)','facecolor',satColor(1,:))
    % bar(bhv.IntervalSet*bhv.Scaling,histc(bhv.Interval(bhv.LeftLong==0),bhv.IntervalSet)','facecolor',satColor(2,:))
    hbar = bar(bhv.IntervalSet*bhv.Scaling,[histc(bhv.Interval(bhv.LeftLong==1),bhv.IntervalSet);...
        histc(bhv.Interval(bhv.LeftLong==0),bhv.IntervalSet)]');
    set(hbar(1),'facecolor',satColor(1,:),'edgecolor','none')
    set(hbar(2),'facecolor',satColor(2,:),'edgecolor','none')
    margins;
    myylim = get(gca,'ylim');
    set(gca,'ylim',[0 myylim(2)],'xtick',bhv.IntervalSet*bhv.Scaling,'xticklabel',bhv.IntervalSet*bhv.Scaling/1000,'tickdir','out')
    
    a=get(gca,'XTickLabel');
    set(gca,'XTickLabel',[]);
    b=get(gca,'XTick');
    c=get(gca,'YTick');
    th=text(b,repmat(c(1)-.1*(c(2)-c(1)),length(b),1),a,'HorizontalAlignment','right','rotation',90);
    clear a b c th
    title 'Trial types'
end
%% Histogram of sreak lengths
subplot(3,4,4), axis square, hold on
cla
try
    ndxStre = [diff(bhv.StreakPos)<1 false];
    streCount = histc(bhv.StreakPos(ndxStre),[1:max(bhv.StreakPos)]);
    streCorr = nan(size(streCount));
    streBreak = nan(size(streCount));
    
    for iStre = 1:max(bhv.StreakPos)
        streCorr(iStre) = nanmean(bhv.ChoiceCorrect(bhv.StreakPos==iStre));
        streBreak(iStre) = nanmean(bhv.BrokeFix(bhv.StreakPos==iStre));
    end
    streCorr(isnan(streCorr)) = 0;
    streBreak(isnan(streBreak)) = 0;
    
    hbar = bar(streCount/sum(streCount));
    set(hbar,'facecolor',ones(1,3)*.1)
    plot(exppdf([1:max(bhv.StreakPos)],2),'r','linewidth',2)
    plot(streBreak/sum(streBreak),'b','linewidth',2)
    plot(streCorr/sum(streCorr),'g','linewidth',2)
    
    text(max(bhv.StreakPos),max(streCount/sum(streCount)),'P(x)','color',ones(1,3)*.1,'verticalalignment','top','horizontalalignment','right')
    text(max(bhv.StreakPos),.9*max(streCount/sum(streCount)),'pred. P(x)','color',[1 0 0],'verticalalignment','top','horizontalalignment','right')
    text(max(bhv.StreakPos),.8*max(streCount/sum(streCount)),'P(x | break fix)','color',[0 0 1],'verticalalignment','top','horizontalalignment','right')
    text(max(bhv.StreakPos),.7*max(streCount/sum(streCount)),'P(x | correct)','color',[0 .8 0],'verticalalignment','top','horizontalalignment','right')
    
    margins;
    myylim = get(gca,'ylim');
    set(gca,'ylim',[0 myylim(2)],'tickdir','out')
    title 'Streak length'
end

%% Pre-Stim delay
subplot(3,4,6), cla, axis square, hold on
try
    edges = [linspace(0,max([bhv.LatencyStim,bhv.StimMissTime]),20),Inf];
    timeb = histc(bhv.StimMissTime, edges);
    timec = histc(bhv.LatencyStim, edges);
    barc = bar(edges(1:end-1),timec(1:end-1),'g');
    barb = bar(edges(1:end-1),timeb(1:end-1),'r');
    set(get(barb,'Children'),'facealpha',0,'edgealpha',1,'edgecolor','r');
    set(get(barc,'Children'),'facealpha',1,'edgealpha',0);
    margins;
    xlabel 'Fixation time (ms)'
    ylabel Counts
    %         legend({'Broken', 'Completed'},'location','northeast','box','off')
    %         legend boxoff
    margins;
    myylim = get(gca,'ylim');
    set(gca,'ylim',[0 myylim(2)],'tickdir','out')
    title 'Pre-Stim delay'
end

%% Inter-beep delay
subplot(3,4,7), cla, axis square, hold on
try
    %         edges = linspace(0,max(max(dataParsed.FixTime),max(dataParsed.BrokeFixTime)),20);
    edges = [linspace(0,max(max(bhv.IntervalPrecise),max(bhv.BrokeFixTime)),20),Inf];
    timeb = histc(bhv.BrokeFixTime, edges);
    timec = histc(bhv.IntervalPrecise, edges);
    barc = bar(edges(1:end-1),timec(1:end-1),'g');
    barb = bar(edges(1:end-1),timeb(1:end-1),'r');
    set(get(barb,'Children'),'facealpha',0,'edgealpha',1,'edgecolor','r');
    set(get(barc,'Children'),'facealpha',1,'edgealpha',0);
    margins;
    xlabel 'Fixation time (ms)'
    ylabel Counts
    %         legend({'Broken', 'Completed'},'location','northeast','box','off')
    %         legend boxoff
    margins;
    myylim = get(gca,'ylim');
    set(gca,'ylim',[0 myylim(2)],'tickdir','out')
    title 'Stim'
end

%% Feedback delay
subplot(3,4,8), cla, axis square, hold on
try
    edges = [linspace(0,max([bhv.LatencyFeedback,bhv.FeedbackMissTime]),20),Inf];
    timeb = histc(bhv.FeedbackMissTime, edges);
    timec = histc(bhv.LatencyFeedback, edges);
    barc = bar(edges(1:end-1),timec(1:end-1),'g');
    barb = bar(edges(1:end-1),timeb(1:end-1),'r');
    set(get(barb,'Children'),'facealpha',0,'edgealpha',1,'edgecolor','r');
    set(get(barc,'Children'),'facealpha',1,'edgealpha',0);
    margins;
    xlabel 'Fixation time (ms)'
    ylabel Counts
    %         legend({'Broken', 'Completed'},'location','northeast','box','off')
    %         legend boxoff
    margins;
    myylim = get(gca,'ylim');
    set(gca,'ylim',[0 myylim(2)],'tickdir','out')
    title 'Feedback delay'
    myPos = get(gca,'position');
    hInset = axes('position',[myPos(1)+myPos(3)/2 myPos(2)+myPos(4)/2 myPos(3)/2 myPos(3)/4]);
    conf = nan(size(bhv.IntervalSet));
    for iInt = 1:numel(conf)
        ndxI = bhv.Interval == bhv.IntervalSet(iInt);
        nanNdx = isnan(bhv.FeedbackMiss);
        conf(iInt) = nanmean(bhv.FeedbackMiss(ndxI));
    end
    plot(bhv.IntervalSet,conf,'r','linewidth',2) % Probability of aborting trial as function of stimulus
    axis square off tight
end

%% Cumulative distributions of choice
subplot(3,4,10), cla, axis square, hold on
try
    if any(bhv.RawData(:,1)==140)
        plot(1:length(bhv.ChoiceLong), cumsum(bhv.ChoiceLong==1), 'color',satColor(1,:),'linewidth',2)
        plot(1:length(bhv.ChoiceLong), cumsum(bhv.ChoiceLong==0), 'color',satColor(2,:),'linewidth',2)
    else
        plot(1:length(bhv.ChoiceLeft), cumsum(bhv.ChoiceLeft==1), 'color',satColor(1,:),'linewidth',2)
        plot(1:length(bhv.ChoiceLeft), cumsum(bhv.ChoiceLeft==0), 'color',satColor(2,:),'linewidth',2)
    end
    xlabel('Trial number', 'FontSize', 10, 'FontWeight', 'bold' )
    % ylabel('Choice number', 'FontSize', 10, 'FontWeight', 'bold' )
    hold off
    text(0,sum(bhv.ChoiceLeft==1),'Long','color',satColor(1,:),'verticalalignment','top', 'Fontsize', 8)
    text(0,sum(bhv.ChoiceLeft==1)*.75,'Short','color',satColor(2,:),'verticalalignment','top', 'Fontsize', 8)
    %     leg = legend('Left', 'Right', 'Location', 'Best');
    %     set(leg, 'Box', 'off', 'Fontsize', 8)
    margins;
    title Biased?
end
%% Gallistel's
subplot(3,4,11)
cla
try
    x = bhv.ChoiceCorrect(~isnan(bhv.ChoiceCorrect));
    x(x==0) = -1;
    plot(cumsum(x),'linewidth',2);
    xlabel({'Trial #';'(valid only)'})
    title 'Cumulative perf'
    margins;
    set(gca,'tickdir','out','box','off')
    axis square
end
%% Trial onsets distribution
subplot(3,4,12), axis square, hold on
cla
try
    cum_ton = bhv.TrialAvail/60000;
    %     plot(cum_ton,[1:length(cum_ton)],'linewidth',2);
    plot3(cum_ton(incorrect_ndx),find(incorrect_ndx),find(incorrect_ndx),'r.','MarkerSize',6);
    plot3(cum_ton(correct_ndx),find(correct_ndx),find(correct_ndx),'g.','MarkerSize',6);
    % axis([0 max(cum_ton) 1 length(dataParsed)])
    set(gca,'box','off','tickdir','out')
    ylabel '# of trials';
    xlabel 'Time (min)'
    axis tight
    %     legend({'All' 'Correct' 'Errors'},'location','southeast');
    %     legend boxoff
    margins;
    title 'Trial rate'
end
%% Output
varargout{1} = nanmean(avgValid);
varargout{2} = hFig;

% %% Reaction Times
% [rt, rtsem, stimSet] = reactime(bhv,false);
% subplot(3,4,3), axis square, hold on
%
% %     patch([stimSet, fliplr(stimSet)],[rt(1,:)-rtsem(1,:), fliplr(rt(1,:)+rtsem(1,:))],[1 172 235]/255,'edgecolor','none','facealpha',0.2)
% %     patch([stimSet, fliplr(stimSet)],[rt(2,:)-rtsem(2,:), fliplr(rt(2,:)+rtsem(2,:))],'g','edgecolor','none','facealpha',0.2)
% %     patch([stimSet, fliplr(stimSet)],[rt(3,:)-rtsem(3,:), fliplr(rt(3,:)+rtsem(3,:))],'r','edgecolor','none','facealpha',0.2)
%
% plot(stimSet,rt(1,:),'color',[1 172 235]/255)
% plot(stimSet,rt(2,:),'g')
% plot(stimSet,rt(3,:),'r')
%
% set(gca,'box','off','tickdir','out','xtick',myxtick,'xticklabel',myxticklabel,'ytick',round(linspace(min(min(rt))-200, max(max(rt))+200, 3)), 'color',[1 1 1])
% axis([0 1 min(min(rt))-200, max(max(rt))+200])
% xlabel 'Interval duration (s)'; ylabel 'Reaction time (ms)'