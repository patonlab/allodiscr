#include <BoxConfig.h>
#include <Utility.h>

/*BoxConfig.h specifies PinOut and valve opening times.
  It is box-specific, but not task-specific.*/
//--------------------------Parameters-----------------------------------
//Task variables
int scaling = 3000; // (ms)
int pitch = 10500;
double initSet[] = {0.2, 0.8};
int bound = scaling / 2;
unsigned long ITI = 3000;
unsigned long timeOutE = 10000; // For incorrect responses
unsigned long timeOutFx = 2500; // For broken fixations
unsigned long choiceDeadline = 600000;
boolean biasedSampling = true;
int delayStim[] = {0, 1, 5000}; //{min,mean,max}
int delayFeedback[] = {30, 50, 170}; //{min,mean,max}

// Internal parameters
int delayStim_now;
int delayFeedback_now;
int state;
int feedbackState;
boolean valuePkC;
boolean valuePkC1;
boolean valuePkC2;
boolean valuePkL;
boolean valuePkS;
boolean valuePkC1now;
boolean valuePkC2now;
boolean valuePkLnow;
boolean valuePkSnow;
boolean stimLong;
boolean leftLong;
unsigned long swTrialOn;
unsigned long swError;
unsigned long swPkIn;
unsigned long swRwdOn;
int trialNum = 0;
int stimCurrent;
int ttNdx; // Trial type index
const int nStim = sizeof(initSet) / sizeof(double);
int stimSet[nStim];
double ttProb[nStim * 2];
double perf[nStim * 2]; // for leftLong trials and !leftLong trials
int seed;
int timeOutEnow;
int timeOutFxnow;
int pkC;
int ledC;
int ledC_on;
int ledC_off;
String strToPrint;

//----------------------------SETUP----------------------------------
// the setup routine runs once when you press reset:
void setup() {
  for (int i = 0; i < nStim; i++) {
    stimSet[i] = int(initSet[i] * scaling);
    perf[i] = 0; // Initialize to 0 to promote 'exploration'
    perf[i + nStim] = 0; // Initialize to 0 to promote 'exploration'
  }

  pinMode(0, INPUT);
  pinMode(syncLed, OUTPUT);

  pinMode(laser, OUTPUT);
  pinMode(syncEphys, OUTPUT);
  pinMode(syncStim, OUTPUT);

  pinMode(spkr, OUTPUT);
  pinMode(pkC1, INPUT);
  pinMode(ledC1, OUTPUT);
  pinMode(pkC2, INPUT);
  pinMode(ledC2, OUTPUT);
  pinMode(pkL, INPUT);
  pinMode(ledL, OUTPUT);
  pinMode(valvL, OUTPUT);
  pinMode(pkS, INPUT);
  pinMode(ledS, OUTPUT);
  pinMode(valvS, OUTPUT);

  digitalWrite(ledL, HIGH);
  digitalWrite(ledC1, HIGH);
  digitalWrite(ledC2, HIGH);
  digitalWrite(ledS, HIGH);

  digitalWrite(syncLed, HIGH);

  digitalWrite(syncEphys, LOW);

  digitalWrite(syncStim, LOW);
  analogWrite(laser, 0);

  digitalWrite(valvL, LOW);
  digitalWrite(valvS, LOW);

  Serial.begin(115200);
  WaitForPyUI();

  seed = RandomizeSeed();

  TCCR5B = (TCCR5B & 0xF8) | 0x01; // set Timer 5 to 32kHz (pins 44) //http://sobisource.com/?p=195

  strToPrint = String(70) + '\t' + String(millis());
  Serial.println(strToPrint);
  strToPrint = String(106) + '\t' + String(scaling);
  Serial.println(strToPrint);
  Serial.println(String(114) + '\t' + String(seed));

  state = 22;
  strToPrint = String(state) + '\t' + String(millis());
  Serial.println(strToPrint);
}

void loop() {
  UpdatePokes();
  stateMachine();
}

// Run state machine
void stateMachine() {
  switch (state) {

    case 22: // state_0 state
      // This is a hack. It is not clear why this timer needs to be reset each time.
      //     TCCR5B = (TCCR5B & 0xF8) | 0x01; // set Timer to 32kHz //http://sobisource.com/?p=195

      strToPrint = String(61) + '\t' + String(millis());
      Serial.println(strToPrint);
      digitalWrite(syncLed, LOW);

      strToPrint = String(201) + '\t' + String(millis());
      Serial.println(strToPrint);
      digitalWrite(syncEphys, HIGH);

      delay(100);
      strToPrint = String(62) + '\t' + String(millis());
      Serial.println(strToPrint);
      digitalWrite(syncLed, HIGH);

      strToPrint = String(202) + '\t' + String(millis());
      Serial.println(strToPrint);
      digitalWrite(syncEphys, LOW);

      // Drawing stimulus and feedback delay
      delayStim_now = TruncExpDist(delayStim[0], delayStim[1], delayStim[2]);
      delayFeedback_now = TruncExpDist(delayFeedback[0], delayFeedback[1], delayFeedback[2]);

      // Drawing a stimulus
      if (!biasedSampling) {
        ttNdx = random(2 * nStim);
      }
      else {
        Update_ttProb(ttProb, perf, nStim);
        ttNdx = RandSample(ttProb, 2 * nStim);
      }
      leftLong = ttNdx < nStim; // 1st half refers to leftLong
      stimCurrent = stimSet[ttNdx % nStim]; // % is the modulo
      stimLong = (stimCurrent > bound);

      //Updating trial count
      trialNum++;

      if (leftLong) {
        Serial.println(String(133) + '\t' + String(1));
        pkC = pkC1;
        ledC = ledC1;
        ledC_on = 3;
        ledC_off = 11;
      }
      else {
        Serial.println(String(133) + '\t' + String(0));
        pkC = pkC2;
        ledC = ledC2;
        ledC_on = 142;
        ledC_off = 143;
      }
      strToPrint = String(ledC_on) + '\t' + String(millis());
      Serial.println(strToPrint);
      digitalWrite(ledC, LOW);
      state = 23;
      strToPrint = String(state) + '\t' + String(millis());
      Serial.println(strToPrint);
      break;

    case 23: // wait_Cin state
      if (leftLong) {
        valuePkC = valuePkC1;
      }
      else {
        valuePkC = valuePkC2;
      }
      if (valuePkC) {
        swTrialOn = millis();
        swPkIn = swTrialOn;
        LogEvent(ledC_off, millis());
        digitalWrite(ledC, HIGH);
        state = 231;
        LogEvent(state, millis());
      }
      break;

    case 231: // delay_stim state
      if ((millis() - swPkIn) > (unsigned long)delayStim_now) {
        state = 56;
        LogEvent(state, millis());
      }
      else {
        if (leftLong) {
          valuePkC = valuePkC1;
        }
        else {
          valuePkC = valuePkC2;
        }
        if (valuePkC == false) {
          state = 233;
          LogEvent(state, millis());
        }
      }
      break;

    case 56: // stim_beep1 state
      swPkIn = millis();
      LogEvent(16, millis());
      tone(spkr, pitch, 150);
      state = 73;
      LogEvent(state, millis());
      break;

    case 73: // stay_Cin state
      if ((millis() - swPkIn) > (unsigned long)stimCurrent) {
        digitalWrite(ledL, LOW);
        LogEvent(4, millis());
        digitalWrite(ledS, LOW);
        LogEvent(5, millis());
        state = 57;
        LogEvent(state, millis());
      }
      else {
        if (leftLong) {
          valuePkC = valuePkC1;
        }
        else {
          valuePkC = valuePkC2;
        }
        if (valuePkC == false) {
          state = 75;
          LogEvent(state, millis());
        }
      }
      break;

    case 75: // broke_fixation state
      perf[ttNdx] = IterAvg(perf[ttNdx], trialNum, 0);
      swError = millis();
      timeOutFxnow = timeOutFx;
      state = 34;
      LogEvent(state, millis());
      break;

    case 57: // stim_beep2 state
      LogEvent(17, millis());
      tone(spkr, pitch, 150);
      state = 25;
      LogEvent(state, millis());
      break;

    case 25: // wait_Sin state
      if ((millis() - swTrialOn) > choiceDeadline) {
        perf[ttNdx] = IterAvg(perf[ttNdx], trialNum, 0);
        state = 40;
        LogEvent(state, millis());
      }
      else {
        if (valuePkL) {
          swPkIn =  millis();
          if (stimLong) {
            feedbackState = 26;
          }
          else {
            feedbackState = 29;
          }
          digitalWrite(ledL, HIGH);
          LogEvent(12, millis());
          digitalWrite(ledS, HIGH);
          LogEvent(13, millis());
          state = 232;
          LogEvent(state, millis());
        }
        if (valuePkS) {
          swPkIn =  millis();
          if (stimLong) {
            feedbackState = 27;
          }
          else {
            feedbackState = 28;
          }
          digitalWrite(ledL, HIGH);
          LogEvent(12, millis());
          digitalWrite(ledS, HIGH);
          LogEvent(13, millis());
          state = 232;
          LogEvent(state, millis());
        }
      }
      break;

    case 232: // delay_feedback state
      if ((millis() - swPkIn) > (unsigned long)delayFeedback_now) {
        state = feedbackState;
        LogEvent(state, millis());
      }
      else {
        if (!valuePkL && !valuePkS) {
          state = 234;
          LogEvent(state, millis());
        }
      }
      break;

    case 26: // correct_Lin state
      perf[ttNdx] = IterAvg(perf[ttNdx], trialNum, 1);
      state = 203;
      LogEvent(state, millis());
      break;

    case 28: // correct_Rin state
      perf[ttNdx] = IterAvg(perf[ttNdx], trialNum, 1);
      state = 204;
      LogEvent(state, millis());
      break;

    case 203: // reward_L state
      swRwdOn = millis();
      Serial.println(strToPrint);
      digitalWrite(valvL, HIGH);
      strToPrint = String(6) + '\t' + String(millis());
      Serial.println(strToPrint);
      delay(timeValvL);
      digitalWrite(valvL, LOW);
      strToPrint = String(14) + '\t' + String(millis());
      Serial.println(strToPrint);
      state = 32;
      LogEvent(state, millis());
      break;

    case 204: // reward_R state
      swRwdOn = millis();
      digitalWrite(valvS, HIGH);
      strToPrint = String(7) + '\t' + String(millis());
      Serial.println(strToPrint);
      delay(timeValvS);
      digitalWrite(valvS, LOW);
      strToPrint = String(15) + '\t' + String(millis());
      Serial.println(strToPrint);
      state = 32;
      LogEvent(state, millis());
      break;

    case 27: // error_Rin state
      timeOutEnow = timeOutE;
      swError = millis();
      perf[ttNdx] = IterAvg(perf[ttNdx], trialNum, 0);
      state = 34;
      strToPrint = String(state) + '\t' + String(millis());
      Serial.println(strToPrint);
      break;

    case 29: // error_Lin state
      timeOutEnow = timeOutE;
      swError = millis();
      perf[ttNdx] = IterAvg(perf[ttNdx], trialNum, 0);
      state = 34;
      LogEvent(state, millis());
      break;

    case 34: // error_tone state
      digitalWrite(ledL, HIGH);
      strToPrint = String(12) + '\t' + String(millis());
      Serial.println(strToPrint);
      digitalWrite(ledS, HIGH);
      strToPrint = String(13) + '\t' + String(millis());
      Serial.println(strToPrint);
      strToPrint = String(35) + '\t' + String(millis());
      Serial.println(strToPrint);
      while ((millis() - swError) < 150) {
        if (random(2) == 1)
        {
          digitalWrite(spkr, HIGH);
        }
        else
        {
          digitalWrite(spkr, LOW);
        }
      }
      strToPrint = String(36) + '\t' + String(millis());
      Serial.println(strToPrint);
      state = 33;
      LogEvent(state, millis());
      break;

    case 33: // error_timeout state
      state = 32;
      LogEvent(state, millis());
      break;

    case 32: // ITI state
      if ((millis() - swTrialOn) > (ITI + timeOutEnow + timeOutFxnow)) {
        timeOutEnow = 0;
        timeOutFxnow = 0;
        state = 22;
        LogEvent(state, millis());
      }
      break;

    case 40: // choice_miss state
      digitalWrite(ledL, HIGH);
      LogEvent(12, millis());
      digitalWrite(ledS, HIGH);
      LogEvent(13, millis());
      state = 32;
      LogEvent(state, millis());
      break;

    case 233: // stim_miss state
      LogEvent(ledC_off, millis());
      digitalWrite(ledC, HIGH);
      state = 32;
      LogEvent(state, millis());
      break;

    case 234: // feedback_miss state
      digitalWrite(ledL, HIGH);
      LogEvent(12, millis());
      digitalWrite(ledS, HIGH);
      LogEvent(13, millis());
      state = 32;
      LogEvent(state, millis());
      break;

  }
}

// Read pokes
void UpdatePokes() {
  valuePkC1now = digitalRead(pkC1);
  valuePkC2now = digitalRead(pkC2);
  valuePkLnow = digitalRead(pkL);
  valuePkSnow = digitalRead(pkS);
  if (valuePkC1 != valuePkC1now) {
    if (valuePkC1now) {
      Serial.println(String(0) + '\t' + String(millis()));
    }
    else {
      Serial.println(String(8) + '\t' + String(millis()));
    }
    valuePkC1 = valuePkC1now;
  }
  if (valuePkC2 != valuePkC2now) {
    if (valuePkC2now) {
      Serial.println(String(140) + '\t' + String(millis()));
    }
    else {
      Serial.println(String(141) + '\t' + String(millis()));
    }
    valuePkC2 = valuePkC2now;
  }
  if (valuePkL != valuePkLnow) {
    if (valuePkLnow) {
      Serial.println(String(1) + '\t' + String(millis()));
    }
    else {
      Serial.println(String(9) + '\t' + String(millis()));
    }
    valuePkL = valuePkLnow;
  }
  if (valuePkS != valuePkSnow) {
    if (valuePkSnow) {
      Serial.println(String(2) + '\t' + String(millis()));
    }
    else {
      Serial.println(String(10) + '\t' + String(millis()));
    }
    valuePkS = valuePkSnow;
  }
}

void Update_ttProb(double theStimProb[], double thePerf[], int theNstim)
{
  for (int i = 0; i < theNstim * 2; i++) {
    theStimProb[i] = 1 - thePerf[i];
  }
}
